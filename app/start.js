"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var server_1 = require("./server");
var ServerStarter = (function () {
    function ServerStarter() {
        var _this = this;
        this.server = server_1.default.create();
        this.port = process.env.PORT || "3000";
        this.server.express.set("port", this.port);
        this.httpServer = http.createServer(this.server.express);
        this.httpServer.on("listening", function () { return _this.onListening(_this); });
        this.httpServer.on("error", function (error) { return _this.onError(_this, error); });
    }
    ServerStarter.prototype.start = function () {
        this.httpServer.listen(this.port);
    };
    ServerStarter.prototype.onListening = function (starter) {
        var bind = starter.httpServer.address();
        console.log("Listening on port " + starter.port);
    };
    ServerStarter.prototype.onError = function (starter, error) {
        throw error;
    };
    ServerStarter.start = function () {
        var starter = new ServerStarter();
        starter.start();
    };
    return ServerStarter;
}());
var HTTPBind = (function () {
    function HTTPBind() {
    }
    return HTTPBind;
}());
ServerStarter.start();
