"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var TestLogRoute = (function () {
    function TestLogRoute(application, model) {
        var _this = this;
        this.application = application;
        this.model = model;
        this.router = express.Router();
        application.get("/test-log", function (request, response, next) { return _this.handler; });
    }
    TestLogRoute.prototype.handler = function (request, response, next) {
        console.log("Handling TestLog route request");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("content-type", "text/plain");
        response.send(this.model.getBusinessTestLog());
    };
    TestLogRoute.prototype.getRouter = function () {
        return this.router;
    };
    return TestLogRoute;
}());
exports.default = TestLogRoute;
