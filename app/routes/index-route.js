"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var IndexRoute = (function () {
    function IndexRoute(model) {
        var _this = this;
        this.router = express.Router();
        this.model = model;
        this.router.get("/test-log", function (request, response, next) {
            return _this.testLogHandler(model, request, response, next);
        });
        this.router.get("/test-results", function (request, response, next) {
            return _this.testResultsHandler(model, request, response, next);
        });
        this.router.get("/", function (request, response, next) {
            return _this.handler(model, request, response, next);
        });
    }
    IndexRoute.prototype.handler = function (model, request, response, next) {
        console.log("Handling Index route request");
        response.render("index", { title: "Express" });
    };
    IndexRoute.prototype.testLogHandler = function (model, request, response, next) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("content-type", "text/plain");
        response.setHeader("Cache-Control", "no-store");
        response.send(model.getBusinessTestLog().data);
    };
    IndexRoute.prototype.testResultsHandler = function (model, request, response, next) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("content-type", "application/json");
        response.setHeader("Cache-Control", "no-store");
        response.send(model.getBusinessTestResults());
    };
    IndexRoute.prototype.getRouter = function () {
        return this.router;
    };
    return IndexRoute;
}());
exports.default = IndexRoute;
