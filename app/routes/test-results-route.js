"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var TestResultsRoute = (function () {
    function TestResultsRoute(model) {
        this.model = model;
        this.router = express.Router();
        this.router.get("/test-results", this.handler);
    }
    TestResultsRoute.prototype.handler = function (request, response, next) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("content-type", "text/plain");
        response.send(this.model.getBusinessTestResults());
    };
    TestResultsRoute.prototype.getRouter = function () {
        return this.router;
    };
    return TestResultsRoute;
}());
exports.default = TestResultsRoute;
