"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var logger = require("morgan");
var model_1 = require("./model");
var index_route_1 = require("./routes/index-route");
var Server = (function () {
    function Server() {
        console.log("Initializing server");
        this.express = express();
        this.model = new model_1.default();
        this.configure();
    }
    Server.prototype.configure = function () {
        console.log("Setting up view engine for directory: " + path.join(__dirname, "views"));
        this.express.set("views", path.join(__dirname, "views"));
        this.express.set("view engine", "hbs");
        this.express.use(logger("dev"));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(cookieParser());
        console.log("Setting up routes");
        this.express.use(express.static(path.join(__dirname, "public")));
        this.express.use("/", new index_route_1.default(this.model).getRouter());
    };
    Server.prototype.fallbackHandler = function (request, response, next) {
        var error = new HTTPError("Not Found");
        error.status = 404;
        next(error);
    };
    Server.create = function () {
        return new Server();
    };
    return Server;
}());
exports.default = Server;
var HTTPError = (function (_super) {
    __extends(HTTPError, _super);
    function HTTPError() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return HTTPError;
}(Error));
