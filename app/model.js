"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var Model = (function () {
    function Model() {
        this.loadConfiguration();
    }
    Model.prototype.loadConfiguration = function () {
        this.configuration = JSON.parse(fs.readFileSync("plsunit-server.json", "utf8"));
        if (!this.configuration.logFileDirectory)
            if (process.env.PROJECT_DISK)
                this.configuration.logFileDirectory = path.join(process.env.PROJECT_DISK, "log", "db");
            else
                this.configuration.logFileDirectory = ".";
        if (!this.configuration.logFilePattern)
            this.configuration.logFilePattern = /BusinessTestResults.log/;
        console.log("- Log file directory: " + this.configuration.logFileDirectory);
        console.log("- Log file pattern: " + this.configuration.logFilePattern);
    };
    Model.prototype.getBusinessTestLog = function () {
        var directory = this.configuration.logFileDirectory;
        var fileName = "BusinessTestResults.log";
        var files = fs.readdirSync(directory);
        var selectedPath;
        var selectedTime;
        var currentFile;
        var currentPath;
        var currentTime;
        for (var i = 0; i < files.length; ++i) {
            currentFile = files[i];
            if (currentFile != null && currentFile.match(this.configuration.logFilePattern)) {
                currentPath = path.join(directory, currentFile);
                currentTime = fs.statSync(currentPath).mtime;
                if (selectedTime == null || currentTime > selectedTime) {
                    selectedPath = currentPath;
                    selectedTime = currentTime;
                }
            }
        }
        if (selectedPath == null)
            throw new Error("Could not find the business test results output file (\"" + this.configuration.logFilePattern + "\")");
        return {
            date: selectedTime,
            data: fs.readFileSync(selectedPath).toString()
        };
    };
    Model.prototype.getBusinessTestResults = function () {
        var result = {
            date: null,
            total: 0,
            failures: 0
        };
        var log = this.getBusinessTestLog();
        var data = log.data;
        data.split("\n").forEach(function (line) {
            if (line.match(/.*OK\s+[0-9\.\,]+\sms.*/)) {
                ++result.total;
            }
            else if (line.match(/.*ERR\s+[0-9\.\,]*\sms.*/)) {
                ++result.total;
                ++result.failures;
            }
        });
        result.date = log.date;
        return result;
    };
    return Model;
}());
exports.default = Model;
var AppConfiguration = (function () {
    function AppConfiguration() {
    }
    return AppConfiguration;
}());
var BusinessTestLog = (function () {
    function BusinessTestLog() {
    }
    return BusinessTestLog;
}());
var BusinessTestResults = (function () {
    function BusinessTestResults() {
    }
    return BusinessTestResults;
}());
