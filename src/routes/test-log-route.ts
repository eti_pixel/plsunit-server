import * as express from "express";
import Model from "../model";

export default class TestLogRoute
{
	private application : express.Application;
	private router : express.Router;
	private model : Model;

	public constructor(application : express.Application, model : Model)
	{
		this.application = application;
		this.model = model;
		this.router = express.Router();
		application.get("/test-log", (request : express.Request, response : express.Response, next : express.NextFunction) => this.handler);
		//express.get("/test-log", this.handler);
	}

	private handler(request : express.Request, response : express.Response, next : express.NextFunction) : void
	{
		console.log("Handling TestLog route request");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("content-type", "text/plain");
		response.send(this.model.getBusinessTestLog());
	}

	public getRouter() : express.Router
	{
		return this.router;
	}
}