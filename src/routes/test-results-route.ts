import * as express from "express";
import Model from "../model";

export default class TestResultsRoute
{
	private router : express.Router;
	private model : Model;

	public constructor(model : Model)
	{
		this.model = model;
		this.router = express.Router();
		this.router.get("/test-results", this.handler);
	}

	private handler(request : express.Request, response : express.Response, next : express.NextFunction) : void
	{
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("content-type", "text/plain");
		response.send(this.model.getBusinessTestResults());
	}

	public getRouter() : express.Router
	{
		return this.router;
	}
}