import * as fs from "fs";
import * as path from "path";

export default class Model
{
	private configuration : AppConfiguration;

	public constructor()
	{
		this.loadConfiguration();
	}

	protected loadConfiguration()
	{
		this.configuration = JSON.parse(fs.readFileSync("plsunit-server.json", "utf8"));

		if (!this.configuration.logFileDirectory)
		{
			console.log("No directory defined; falling back to default");

			if (process.env.PROJECT_DISK)
				this.configuration.logFileDirectory = path.join(process.env.PROJECT_DISK, "log", "db");
			else
				this.configuration.logFileDirectory = ".";
		}

		if (!this.configuration.logFilePattern)
		{
			console.log("No file pattern defined; falling back to default");
			this.configuration.logFilePattern = /BusinessTestResults.log/;
		}

		console.log("- Log file directory: " + this.configuration.logFileDirectory);
		console.log("- Log file pattern: " + this.configuration.logFilePattern);
	}

	public getBusinessTestLog() : BusinessTestLog
	{
		var directory : string = this.configuration.logFileDirectory;
		var files : string[] = fs.readdirSync(directory);
		var selectedPath : string;
		var selectedTime : Date;
		var currentFile : string;
		var currentPath : string;
		var currentTime : Date;

		for (var i : number = 0; i < files.length; ++i)
		{
			currentFile = files[i];

			if (currentFile != null && currentFile.match(this.configuration.logFilePattern))
			{
				currentPath = path.join(directory, currentFile);
				currentTime = fs.statSync(currentPath).mtime;

				if (selectedTime == null || currentTime > selectedTime)
				{
					selectedPath = currentPath;
					selectedTime = currentTime;
				}
			}
		}

		if (selectedPath == null)
			throw new Error("Could not find the business test results output file (\"" + this.configuration.logFilePattern + "\")");

		return {
			date : selectedTime,
			data : fs.readFileSync(selectedPath).toString()
		};
	}

	public getBusinessTestResults() : BusinessTestResults
	{
		var result : BusinessTestResults = {
			date: null,
			total: 0,
			failures: 0
		};

		var log : BusinessTestLog = this.getBusinessTestLog();
		var data : string = log.data;

		data.split("\n").forEach((line) => {
			if (line.match(/.*OK\s+[0-9\.\,]+\sm?s?.*/))
			{
				++result.total;
			}
			else if (line.match(/.*ERR\s+[0-9\.\,]*\sm?s?.*/))
			{
				++result.total;
				++result.failures;
			}
		});

		result.date = log.date;

		return result;
	}
}

class AppConfiguration
{
	public logFileDirectory : string;
	public logFilePattern : { [Symbol.match](string: string): RegExpMatchArray | null };
}

class BusinessTestLog
{
	public date : Date;
	public data : string;
}

class BusinessTestResults
{
	public date : Date;
	public total : number;
	public failures : number;
}